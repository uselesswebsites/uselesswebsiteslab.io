module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy('src/*.css')
    eleventyConfig.addPassthroughCopy('src/*.ico')
    eleventyConfig.addPassthroughCopy('src/*.png')
    return {
        passthroughFileCopy: true
    }
}
